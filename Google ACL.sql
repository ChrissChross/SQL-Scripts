BEGIN
   DBMS_NETWORK_ACL_ADMIN.create_acl (
        acl          => 'google_acl_file.xml', 
        description  => 'ACL to grant access to google server',
        principal    => 'APEX_190100',
        is_grant     => TRUE, 
        privilege    => 'connect',
        start_date   => SYSTIMESTAMP,
        end_date     => NULL);

    DBMS_NETWORK_ACL_ADMIN.assign_acl (
        acl         => 'google_acl_file.xml',
        host        => 'accounts.google.com', 
        lower_port  => 443,
        upper_port  => NULL);
    
    DBMS_NETWORK_ACL_ADMIN.ADD_PRIVILEGE(
        acl         => 'google_acl_file.xml',
        principal   => 'APEX_190100',
        is_grant    =>  TRUE,
        privilege   => 'connect');
  COMMIT;
END;
/